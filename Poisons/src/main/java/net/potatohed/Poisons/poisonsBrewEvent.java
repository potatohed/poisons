package net.potatohed.Poisons;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class poisonsBrewEvent implements Listener 
{
	
	@EventHandler
	public void onBrewEvent(BrewEvent e)
	{
		BrewerInventory bi = e.getContents();
		ItemStack[] content = bi.getContents();
		for (ItemStack item : content)
		{
			if (item.getType() == Material.POTION)
			{
				if (item.getItemMeta().hasLore())
				{
					return;
				}
				else
				{
					ItemMeta imeta = item.getItemMeta();
					List<String> ilore = new LinkedList<String>();
					ilore.add("�l");
					imeta.setLore(ilore);
					item.setItemMeta(imeta);
				}
			}
		}
	}

}
